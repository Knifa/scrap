const http = require('http')
const path = require('path')
const fs = require('fs')

const chokidar = require('chokidar')
const webpack = require('webpack')
const webpackConfig = require('../webpack.config.js')
const pug = require('pug')


function httpHandler(req, res) {
  if (req.url.startsWith('/static/')) {
    const filepath = req.url.replace('/static/', '')
    if (filepath.includes('..')) {
      res.end()
      return
    }

    fs.createReadStream(path.join(__dirname, '../static/', filepath))
      .on('error', () => { res.end() })
      .pipe(res)
  } else {
    switch (req.url) {
      case '/':
        fs.createReadStream(path.join(__dirname, '../bin/index.html')).pipe(res)
        break

      case '/bundle.js':
        fs.createReadStream(path.join(__dirname, '../bin/bundle.js')).pipe(res)
        break

      case '/bundle.js.map':
        fs.createReadStream(path.join(__dirname, '../bin/bundle.js.map'))
          .pipe(res)
        break

      default:
        res.end()
        break
    }
  }
}

function webpackHandler(err) {
  if (err)
    console.log(err)
  else
    console.log('Bundle compiled.')
}


function pugHandler() {
  const html = pug.renderFile(path.join(__dirname, '../src/pug/index.pug'))
  fs.writeFileSync(path.join(__dirname, '../bin/index.html'), html)
  console.log('HTML compiled.')
}


webpack(webpackConfig).watch(undefined, webpackHandler)
pugHandler()

http.createServer(httpHandler).listen(8080)
chokidar.watch(path.join(__dirname, '../src/pug/')).on('change', pugHandler)

import '../scss/index.scss'


/*
class ShipTrail extends GameEntity {
  constructor() {
    super()

    this.addComponent(new TransformComponent())
    this.addComponent(new AttachComponent())
    this.addComponent(new TrailComponent())
  }
}*/


import Game from 'engine/Game'
import RunMode from 'engine/RunMode'

import GameGraphics from 'engine/cores/graphics/GameGraphics'
import GameInput from 'engine/cores/GameInput'
import NetClientCore from 'engine/cores/net/client/NetClientCore'

import SpriteRenderingSystem from 'engine/systems/SpriteRenderingSystem'

import TransformComponent from 'engine/components/TransformComponent'
import SpriteComponent from 'engine/components/SpriteComponent'
import OwnedByPlayerComponent from 'engine/components/OwnedByPlayerComponent'
import PhysicsComponent from 'engine/components/PhysicsComponent'
import AttachComponent from 'engine/components/AttachComponent'
import TrailComponent from 'engine/components/TrailComponent'

import TrailSystem from 'engine/systems/TrailSystem'

import CameraFollowerComponent from 'game/components/CameraFollowerComponent'
import ShipMovementComponent from 'game/components/ShipMovementComponent'
import BackgroundScrollComponent from 'game/components/BackgroundScrollComponent'

import CameraFollowerSystem from 'game/systems/CameraFollowerSystem'
import BackgroundScrollSystem from 'game/systems/BackgroundScrollSystem'


const game = new Game({
  runMode: RunMode.CLIENT,

  cores: [
    GameGraphics,
    GameInput,
    NetClientCore
  ],

  systems: [
    SpriteRenderingSystem,
    CameraFollowerSystem,
    BackgroundScrollSystem,
    TrailSystem
  ],

  components: [
    TransformComponent,
    SpriteComponent,
    OwnedByPlayerComponent,
    PhysicsComponent,
    ShipMovementComponent,
    CameraFollowerComponent,
    BackgroundScrollComponent,
    AttachComponent,
    TrailComponent
  ]
})

game.start()

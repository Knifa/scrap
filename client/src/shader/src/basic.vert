attribute vec3 aVertexPosition;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMVMatrix;

varying vec4 vVertexPosition;


void main() {
  vVertexPosition = vec4(aVertexPosition, 1.0);

  gl_Position =
    uPMatrix *
    uVMatrix *
    uMVMatrix *
    vec4(aVertexPosition, 1.0);
}

precision highp float;


#define BLUR_OFFSET 4
#define BLUR_TAPS 5


varying vec4 vVertexPosition;

uniform ivec2 iResolution;
uniform sampler2D uFramebuffer;
uniform sampler2D uOrigFramebuffer;


vec4 blur(in sampler2D tex, in vec2 uv) {
  vec2 blurOffsetUv = vec2(1.0, 1.0) / float(iResolution) * float(BLUR_OFFSET);
  float ar = float(iResolution.x) / float(iResolution.y);

  vec4 sum = vec4(0.0);
  for (int x = -BLUR_TAPS; x <= BLUR_TAPS; x++) {
    for (int y = -BLUR_TAPS; y <= BLUR_TAPS; y++) {
      sum += texture2D(
        tex,
        vec2(
          uv.x + (float(x) / float(BLUR_TAPS) * blurOffsetUv.x),
          uv.y + (float(y) / float(BLUR_TAPS) * blurOffsetUv.y * ar)
        )
      );
    }
  }

  return sum / pow(float(BLUR_TAPS * 2 + 1), 2.0);
}


void main() {
  vec2 uv = (vec2(vVertexPosition) + 0.5);

  vec4 blurred = blur(uFramebuffer, uv);
  vec4 orig = texture2D(uFramebuffer, uv);

  gl_FragColor = blurred;
}

precision highp float;


#define BLUR_OFFSET 16
#define BLUR_TAPS 5
#define POWER 2.0

varying vec4 vVertexPosition;

uniform ivec2 iResolution;

uniform sampler2D uOrigFramebuffer;
uniform sampler2D uFramebuffer;


vec4 blur(in sampler2D tex, in vec2 uv, vec2 strength) {
  vec2 blurOffsetUv = vec2(1.0, 1.0) / float(iResolution) * float(BLUR_OFFSET) * strength;
  float ar = float(iResolution.x) / float(iResolution.y);

  vec4 sum = vec4(0.0);
  for (int x = -BLUR_TAPS; x <= BLUR_TAPS; x++) {
    for (int y = -BLUR_TAPS; y <= BLUR_TAPS; y++) {
      sum += texture2D(
        tex,
        vec2(
          min(uv.x + (float(x) / float(BLUR_TAPS) * blurOffsetUv.x), 1.0),
          min(uv.y + (float(y) / float(BLUR_TAPS) * blurOffsetUv.y * ar), 1.0)
        )
      );
    }
  }

  return sum / pow(float(BLUR_TAPS * 2 + 1), 2.0);
}

vec3 distort(sampler2D tex, vec2 uv, float o) {
  float ar = float(iResolution.x) / float(iResolution.y);
  float r2 = distance(uv, vec2(0.0, 0.0));
  float fx = 1.0 + r2 * o;
  float fy = 1.0 + r2 * o / ar;

  return blur(
    tex,
    uv * vec2(fx, fy) / 2.0 + 0.5,
    vec2(max(pow(r2, 2.0) - 1.0, 0.0), max(pow(r2, 2.0) - 1.0, 0.0))
  ).rgb;
}


void main() {
  vec2 uv2 = vVertexPosition.xy * 2.0;

  float r = distort(uFramebuffer, uv2, -0.01 * POWER).r;
  float g = distort(uFramebuffer, uv2, -0.015 * POWER).g;
  float b = distort(uFramebuffer, uv2, -0.02 * POWER).b;

  gl_FragColor =
    vec4(
      vec3(r, g, b),
      1.0
    );
}

#extension GL_OES_standard_derivatives : enable


precision highp float;

uniform ivec2 uResolution;
varying vec4 vVertexPosition;
uniform vec2 uOffset;


#define GRID_SIZE 256.0


void main() {
  float ar = float(uResolution.x) / float(uResolution.y);
  vec2 centerOffset = vec2(float(uResolution.x) / 2.0, float(uResolution.x) / 2.0 / ar);
  vec2 gridPos = (gl_FragCoord.xy + uOffset - centerOffset) / GRID_SIZE;

  // Compute anti-aliased world-space grid lines
  vec2 grid = abs(fract(gridPos + 0.25) - 0.25) / fwidth(gridPos);
  float line = min(grid.x, grid.y);

  // Just visualize the grid lines directly
  gl_FragColor = vec4(vec3(1.0 - line) * 0.25, 1.0);
}

precision highp float;


varying vec4 vVertexPosition;

uniform sampler2D uTexture;
uniform vec2 uScale;
uniform vec2 uOffset;


void main() {
  vec2 uv = (vVertexPosition.xy + uOffset) / uScale + 0.5;

  gl_FragColor = texture2D(uTexture, vec2(-uv.x, uv.y));
}

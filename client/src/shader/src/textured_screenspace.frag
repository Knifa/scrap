precision highp float;

varying vec4 vVertexPosition;

uniform sampler2D uTexture;

uniform ivec2 uResolution;
uniform vec2 uScale;
uniform vec2 uOffset;


void main() {
  float ar = float(uResolution.x) / float(uResolution.y);
  vec2 centerFrag = vec2(
    (gl_FragCoord.x + uOffset.x - float(uResolution.x) / 2.0) / float(uResolution.x),
    (gl_FragCoord.y + uOffset.y - float(uResolution.y) / 2.0) / float(uResolution.y)
  );

  vec2 fragCoordUv = vec2(
    centerFrag.x / uScale.x - 0.5,
    centerFrag.y / uScale.y / ar - 0.5
  );

  gl_FragColor = texture2D(uTexture, fragCoordUv);
}

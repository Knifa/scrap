precision highp float;


varying vec4 vVertexPosition;
varying vec2 vVertexUv;
varying vec4 vVertexColor;

uniform sampler2D uTexture;
uniform vec2 uScale;
uniform vec2 uOffset;


void main() {
  gl_FragColor = texture2D(uTexture, vVertexUv) * vVertexColor;
}

attribute vec3 aVertexPosition;
attribute vec2 aVertexUv;
attribute vec4 aVertexColor;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMVMatrix;

varying vec4 vVertexPosition;
varying vec2 vVertexUv;
varying vec4 vVertexColor;


void main() {
  vVertexPosition = vec4(aVertexPosition, 1.0);
  vVertexUv = aVertexUv;
  vVertexColor = aVertexColor;

  gl_Position =
    uPMatrix *
    uVMatrix *
    uMVMatrix *
    vec4(aVertexPosition, 1.0);
}

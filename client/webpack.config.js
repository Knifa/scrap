const path = require('path')
const autoprefixer = require('autoprefixer')


module.exports = {
  context: path.resolve(__dirname, './src/js'),
  entry: ['babel-polyfill', path.resolve(__dirname, './src/js/index.js')],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './bin')
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
        query: {
          presets: require.resolve('babel-preset-es2015')
        }
      },
      {
        test: /\.scss$/,
        loaders: [
          require.resolve('style-loader'),
          require.resolve('css-loader'),
          require.resolve('postcss-loader'),
          require.resolve('sass-loader')
        ]
      },
      {
        test: /\.(vert|frag)$/,
        loader: require.resolve('raw-loader')
      },
      {
        test: /\.json$/,
        loader: require.resolve('json-loader')
      },
      {
        test: /\.(png|jpg)$/,
        loader: require.resolve('url-loader')
      }
    ]
  },

  resolve: {
    root: [
      path.resolve(__dirname, './src'),
      path.resolve(__dirname, '../common'),
      path.resolve(__dirname, './node_modules')
    ]
  },

  postcss: [autoprefixer],
  devtool: 'source-map'
}

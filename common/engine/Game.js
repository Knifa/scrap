import EventEmitter from 'events'

import GameEntity from './GameEntity'
import RunMode from './RunMode'


class _TimeTracker {
  constructor(game) {
    this.game = game

    this._lastT = null

    this.t = 0
    this.dt = 0
  }

  tick() {
    if (this.game.runMode === RunMode.CLIENT) {
      const now = window.performance.now() / 1000

      if (this._lastT) {
        this.t = now
        this.dt = now - this._lastT
      }

      this._lastT = now
    } else {
      const now = process.hrtime()

      if (this._lastT) {
        this.t = now[0] + now[1] * 1e-9
        this.dt =
          (now[0] + now[1] * 1e-9) -
          (this._lastT[0] + this._lastT[1] * 1e-9)
      }

      this._lastT = now
    }

    return this.getState()
  }

  getState() {
    return {
      t: this.t,
      dt: this.dt
    }
  }
}


export default class Game {
  /**
   * @param {Object} options init options
   * @param {RunMode} options.runMode game run mode
   * @param {Object[]) options.cores list of game cores
   * @param {Object[]) options.systems list of game systems
   * @param {Object[]) options.components list of game components
   */
  constructor(options) {
    this.runMode = options.runMode
    this._loopInit = options.loopInit
    this._loopTrigger = options.loopTrigger

    this.time = new _TimeTracker(this)
    this.events = new EventEmitter()

    this.entities = []

    this.cores = {}
    this.systems = {}
    this.components = {}

    for (const core of options.cores)
      this.addCore(core)

    for (const system of options.systems)
      this.addSystem(system)

    for (const component of options.components)
      this.registerComponent(component)
  }

  start() {
    this.events.emit('core:start')

    if (this.runMode === RunMode.CLIENT) {
      window.requestAnimationFrame(() => this.loop())
    } else {
      setInterval(() => this.loop(), 1000 / 64)
    }
  }

  loop() {
    const tickTime = this.time.tick()

    this.events.emit('core:preTick')
    this.events.emit('system:preTick', tickTime)

    this.events.emit('core:tick', tickTime)
    this.events.emit('system:tick', tickTime)

    this.events.emit('system:postTick', tickTime)
    this.events.emit('core:postTick')


    if (this.runMode === RunMode.CLIENT) {
      window.requestAnimationFrame(() => this.loop())
    }
  }

  addCore(cClass) {
    if (cClass.runsOn && cClass.runsOn !== this.runMode)
      return

    this.cores[cClass.friendlyName] = new cClass(this)
  }

  addSystem(sClass) {
    if (sClass.runsOn && sClass.runsOn !== this.runMode)
      return

    this.systems[sClass.name] = new sClass(this)
  }

  addEntity(entity) {
    this.entities.push(entity)
  }

  spawnEntity(components) {
    const entity = new GameEntity(this.entities.length, components)

    this.addEntity(entity)
    this.events.emit('core:spawnEntity', entity)

    return entity.id
  }

  registerComponent(cClass) {
    this.components[cClass.name] = cClass
  }

  getEntitiesWithComponents(components) {
    return this.entities.filter(
      (entity) => entity.hasComponents(components)
    )
  }
}

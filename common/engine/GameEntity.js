export default class GameEntity {
  constructor(id, components = []) {
    this.id = id
    this.components = {}

    for (const component of components)
      this.addComponent(component)
  }

  addComponent(component) {
    const name =
      component.constructor.name.slice(0, 1).toLowerCase() +
      component.constructor.name.slice(1)

    const shortName = name.replace('Component', '')

    this[shortName] = component
    this.components[component.constructor.name] = component
  }

  hasComponent(componentClass) {
    return componentClass.name in this.components
  }

  hasComponents(componentClasses) {
    return componentClasses.every(
      (componentClass) => this.hasComponent(componentClass)
    )
  }
}

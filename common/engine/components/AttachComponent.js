import GameComponent from 'engine/GameComponent'


export default class AttachComponent extends GameComponent {
  constructor(options) {
    super()

    this.parentId = options.parentId
    this.childIds = []
  }
}

AttachComponent.netSyncProperties = {
  parentId: 'number'
}

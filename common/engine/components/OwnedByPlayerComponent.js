import GameComponent from '../GameComponent'


export default class OwnedByPlayer extends GameComponent {
  constructor(options) {
    super()

    this.playerId = options.playerId
  }
}

OwnedByPlayer.netSyncProperties = {
  playerId: 'number'
}

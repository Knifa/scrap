import GameComponent from '../GameComponent'
import Vector from '../utils/Vector'

export default class PhysicsComponent extends GameComponent {
  constructor() {
    super()

    this.forces = []
    this.mass = 1
    this.velocity = new Vector()
    this.acceleration = new Vector()
  }
}

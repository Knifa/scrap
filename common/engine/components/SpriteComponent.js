import GameComponent from '../GameComponent'
import BlendFunc from '../cores/graphics/gl/BlendFunc'
import Vector from '../utils/Vector'


export default class SpriteComponent extends GameComponent {
  constructor(options) {
    super()

    this.texturePath = options.texturePath
    this.shaderPath = options.shaderPath

    this.size = options.size ? options.size : new Vector({ x: 512, y: 512 })
    this.textureScale = options.textureScale ? options.textureScale : new Vector({ x: 1, y: 1 })
    this.textureOffset = new Vector({ x: 1, y: 1 })
    this.blendFunc = options.blendFunc ? options.blendFunc : BlendFunc.ALPHA
  }
}

SpriteComponent.FULLSCREEN = new Vector({ x: -1, y: -1 })

SpriteComponent.netSyncProperties = {
  texturePath: 'string',
  shaderPath: 'string',

  size: 'vector',
  textureScale: 'vector',
  textureOffset: 'vector',

  blendFunc: 'string'
}

import GameComponent from 'engine/GameComponent'


export default class TrailComponent extends GameComponent {
  constructor() {
    super()

    this._vertBuffer = null
    this._colBufer = null
    this._uvBuffer = null

    this._verts = []
    this._uv = []
    this._cols = []
    this._lastPos = null

    this.active = true
    this._lastActive = false

    this.color = [1, 1, 1, 1]
  }
}

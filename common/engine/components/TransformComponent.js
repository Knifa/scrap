import GameComponent from '../GameComponent'

import Transform from '../utils/Transform'


export default class TransformComponent extends GameComponent {
  constructor() {
    super()

    this.local = new Transform()
    this.world = new Transform()
  }
}

TransformComponent.netSyncProperties = {
  local: 'transform',
  world: 'transform'
}

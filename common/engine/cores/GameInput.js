import GameCore from '../GameCore'
import RunMode from '../RunMode'


export default class GameInput extends GameCore {
  constructor(game) {
    super(game)

    this.mouse = { x: 0, y: 0, buttonLeft: false }
    this.keys = { }

    this.game.events.on('core:start', () => this.start())
  }

  start() {
    this._elCanvas = window.document.getElementById('canvas')
    this._elCanvas.focus()

    this._elCanvas.addEventListener('mousemove', (e) => this._onMouseMove(e))
    this._elCanvas.addEventListener('mousedown', (e) => this._onMouseDown(e))
    this._elCanvas.addEventListener('mouseup', (e) => this._onMouseUp(e))

    this._elBody = window.document.body
    this._elBody.addEventListener('keydown', (e) => this._onKeyDown(e))
    this._elBody.addEventListener('keyup', (e) => this._onKeyUp(e))
  }

  _onMouseMove(e) {
    this.mouse.x = e.clientX - (this._elCanvas.width / 2)
    this.mouse.y = (this._elCanvas.height / 2) - e.clientY
  }

  _onMouseDown() {
    this.mouse.buttonLeft = true
  }

  _onMouseUp() {
    this.mouse.buttonLeft = false
  }

  _onKeyDown(e) {
    this.keys[e.keyCode] = true
  }

  _onKeyUp(e) {
    delete this.keys[e.keyCode]
  }
}


GameInput.friendlyName = 'input'
GameInput.runsOn = RunMode.CLIENT

import Transform from 'engine/utils/Transform.js'
import { mat4 } from 'gl-matrix'


export default class Camera {
  constructor(gfx) {
    this.gfx = gfx
    this.transform = new Transform()

    this.gfx.gl.viewport(0, 0, this.gfx.canvas.width, this.gfx.canvas.height)
    this.gfx.gl.clearColor(0.0, 0.0, 0.0, 1.0)

    this.projMatrix = mat4.create()
    mat4.ortho(
      this.projMatrix,
      this.gfx.canvas.width / 2, -this.gfx.canvas.width / 2,
      this.gfx.canvas.height / 2, -this.gfx.canvas.height / 2,
      -1.0, 1.0
    )
  }

  getProjectionMatrix() {
    return this.projMatrix
  }

  getViewMatrix() {
    const m = mat4.create()
      mat4.scale(m, m, [this.transform.scale.x, this.transform.scale.y, 1])
    mat4.translate(m, m, [-this.transform.pos.x, -this.transform.pos.y, 0])
    mat4.rotateZ(m, m, this.transform.rot)

    return m
  }

  lookAt(pos) {
    this.transform.pos = pos.clone()
  }
}

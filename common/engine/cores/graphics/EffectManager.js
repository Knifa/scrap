import { mat4, vec3 } from 'gl-matrix'
import { Framebuffer } from './gl'


export default class FullscreenEffect {
  constructor(gfx) {
    this.gfx = gfx
    this.gl = gfx.gl
    this.width = this.gfx.canvas.width
    this.height = this.gfx.canvas.height

    this._fb = new Framebuffer(this.gl, this.width, this.height)
    this._passes = []

    this._pMatrix = mat4.create()
    mat4.ortho(
      this._pMatrix,
      -this.width / 2, this.width / 2,
      -this.height / 2, this.height / 2,
      -1.0, 1.0
    )

    this._mvMatrix = mat4.create()
    mat4.scale(this._mvMatrix, this._mvMatrix,
      vec3.fromValues(this.width, this.height, 1)
    )
  }

  add(name) {
    const ppProgs = require(`shader/postprocess/${name}.json`)
    for (const progName of ppProgs) {
      this._passes.push({
        fb: new Framebuffer(this.gl, this.width, this.height),
        prog: this.gfx.glResources.getShaderProgram(progName)
      })
    }
  }

  prepare() {
    if (this._passes.length == 0)
      return

    this._fb.bind()
    this.gl.clear(this.gl.COLOR_BUFFER_BIT)
  }

  render() {
    if (this._passes.length == 0)
      return

    this.gfx.glResources.getVertexBuffer('sprite').bind()
    this.gfx.gl.blendFunc(this.gfx.gl.ONE, this.gfx.gl.ZERO)

    for (let i = 0; i < this._passes.length; i++) {
      this._fb.texture.use(0)

      if (i === 0) {
        this._fb.texture.use(1)
      } else {
        this._passes[i - 1].fb.texture.use(1)
      }

      if (this._passes.length === 1 || i === this._passes.length - 1) {
        Framebuffer.unbind(this.gl)
      } else {
        this._passes[i].fb.bind()
      }

      this._passes[i].prog.use()
      this._passes[i].prog.setAttributePointer()
      this._passes[i].prog.setUniformMat4f('uPMatrix', this._pMatrix)
      this._passes[i].prog.setUniformMat4f('uVMatrix', mat4.create())
      this._passes[i].prog.setUniformMat4f('uMVMatrix', this._mvMatrix)
      this._passes[i].prog.setUniform2i('iResolution', [this.width, this.height])
      this._passes[i].prog.setUniform1i('uOrigFramebuffer', [0])
      this._passes[i].prog.setUniform1i('uFramebuffer', [1])

      this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4)
    }
  }
}

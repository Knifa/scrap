import GameCore from '../../GameCore'
import RunMode from '../../RunMode'


import Camera from './Camera'
import ResourceManager from './gl/ResourceManager'
import EffectManager from './EffectManager'

import Vector from '../../utils/Vector'


export default class GameGraphics extends GameCore {
  constructor(game) {
    super(game)

    this.game.events.on('core:start', () => this.start())
    this.game.events.on('core:postTick', () => this.postTick())

    this.canvas = document.getElementById('canvas')
    this.canvas.width = document.body.offsetWidth
    this.canvas.height = document.body.offsetHeight
    this.canvasSize = new Vector({
      x: this.canvas.width,
      y: this.canvas.height
    })

    this.gl = this.canvas.getContext('webgl', {
      alpha: false,
      premultipliedAlpha: false
    })

    this.gl.enable(this.gl.BLEND);
    this.glResources = new ResourceManager(this.gl)

    this.camera = new Camera(this)
    this.camera.transform.scale = new Vector({ x: 0.75, y: 0.75 })

    this.effectManager = new EffectManager(this)
  }

  start() {
    this.effectManager.add('chroma')
    this.effectManager.add('bloom')
  }

  postTick() {
    this.effectManager.prepare()
    this.game.events.emit('system:render')
    this.effectManager.render()
  }
}


GameGraphics.friendlyName = 'gfx'
GameGraphics.runsOn = RunMode.CLIENT

import Vector from '../utils/Vector'
import Transform from '../utils/Transform'


class _Particle {
  constructor(pos, velocity) {
    if (!pos) pos = new Vector()
    if (!velocity) velocity = new Vector()

    this.transform = new Transform(pos)
    this.velocity = { x: velocity.x, y: velocity.y }

    this.img = new Image()
    this.img.src = '/static/particle.jpg'

    this.life = 1
  }

  render(ctx) {
    ctx.globalCompositeOperation = 'lighter'
    ctx.globalAlpha = this.life
    ctx.scale(1 / this.img.width * 32, 1 / this.img.height * 32)
    ctx.drawImage(this.img, -this.img.width / 2, -this.img.height / 2)
  }

  update() {
    this.transform.pos = this.transform.pos.add(this.velocity)
    this.life = Math.max(0, this.life - 0.01)
  }

  isAlive() {
    return this.life > 0
  }
}

export default class ParticleSystem {
  constructor() {
    this.particles = []
  }

  emit(pos, velocity) {
    this.particles.push(new _Particle(pos, velocity))
  }

  render(ctx) {
    for (const p of this.particles) {
      ctx.save()
      p.transform.apply(ctx)
      p.render(ctx)
      ctx.restore()
    }
  }

  update() {
    for (const p of this.particles) {
      p.update()
    }

    this.particles = this.particles.filter((p) => p.isAlive())
  }
}

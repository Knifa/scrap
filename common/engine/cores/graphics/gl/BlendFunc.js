export default {
  apply(gl, blendFunc) {
    switch (blendFunc) {
      case 'add':
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE)
        break

      case 'alpha':
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
        break
    }
  },

  ADD: 'add',
  ALPHA: 'alpha'
}

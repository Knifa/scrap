import Texture from './Texture'


export default class Framebuffer {
  constructor(gl, width, height) {
    this.gl = gl

    this.glFramebuffer = this.gl.createFramebuffer()
    this.bind()

    this.texture = new Texture(this.gl)
    this.texture.setDataRaw(width, height, null)
    this.texture.setParameter(this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR)
    this.texture.setParameter(this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR)
    this.texture.setParameter(this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE)
    this.texture.setParameter(this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE)
    this.gl.framebufferTexture2D(
      this.gl.FRAMEBUFFER,
      this.gl.COLOR_ATTACHMENT0,
      this.gl.TEXTURE_2D,
      this.texture.glTexture,
      0
    )

    Framebuffer.unbind(this.gl)
  }

  bind() {
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.glFramebuffer)
  }

  static unbind(gl) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
  }
}

import path from 'path'

import Shader from './Shader'
import ShaderProgram from './ShaderProgram'
import Texture from './Texture'
import VertexBuffer from './VertexBuffer'


export default class ResourceManager {
  constructor(gl) {
    this.gl = gl

    this._shaders = {}
    this._shaderPrograms = {}
    this._textures = {}
    this._vbs = {}
  }

  getTexture(srcPath) {
    if (!this._textures[srcPath]) {
      this._textures[srcPath] = new Texture(this.gl)

      const img = new Image()
      img.src = require(`texture/${srcPath}`)
      img.addEventListener('load', () => {
        this._textures[srcPath].setDataImage(img)
        this._textures[srcPath].setParameter(
          this.gl.TEXTURE_MIN_FILTER,
          this.gl.LINEAR_MIPMAP_LINEAR
        )

        this._textures[srcPath].generateMipmap()
      })
    }

    return this._textures[srcPath]
  }

  getShader(srcPath) {
    if (!this._shaders[srcPath]) {
      this._shaders[srcPath] = new Shader(
        this.gl,
        this._getShaderType(srcPath),
        require(`shader/src/${srcPath}`))
    }

    return this._shaders[srcPath]
  }

  getShaderProgram(name) {
    if (!this._shaderPrograms[name]) {
      const prog = new ShaderProgram(this.gl)
      const desc = require(`shader/program/${name}.json`)

      prog.attach(
        this.getShader(`${desc.vertex}.vert`),
        this.getShader(`${desc.fragment}.frag`)
      )
      prog.link()

      this._shaderPrograms[name] = prog
    }

    return this._shaderPrograms[name]
  }

  getVertexBuffer(name) {
    if (!this._vbs[name]) {
      this._vbs[name] = new VertexBuffer(
        this.gl,
        require(`mesh/${name}.json`).verts
      )
    }

    return this._vbs[name]
  }

  _getShaderType(shaderPath) {
    const EXT_TO_TYPE = {
      '.vert': this.gl.VERTEX_SHADER,
      '.frag': this.gl.FRAGMENT_SHADER
    }

    return EXT_TO_TYPE[path.extname(shaderPath)]
  }
}

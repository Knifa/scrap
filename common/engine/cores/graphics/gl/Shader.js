export default class Shader {
  constructor(gl, type, src) {
    this.glShader = gl.createShader(type)

    gl.shaderSource(this.glShader, src)
    gl.compileShader(this.glShader)

    if (!gl.getShaderParameter(this.glShader, gl.COMPILE_STATUS)) {
      console.log(gl.getShaderInfoLog(this.glShader))
    }
  }
}

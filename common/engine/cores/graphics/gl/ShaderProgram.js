class _Shader {
  constructor(gl, type, src) {
    this.glShader = gl.createShader(type)

    gl.shaderSource(this.glShader, src)
    gl.compileShader(this.glShader)

    if (!gl.getShaderParameter(this.glShader, gl.COMPILE_STATUS)) {
      console.log(gl.getShaderInfoLog(this.glShader))
    }
  }
}


export default class ShaderProgram {
  constructor(gl) {
    this.gl = gl
    this.glProgram = this.gl.createProgram()

    this._linked = false
    this._attrs = {}
    this._uniforms = {}
  }


  attach(...shaders) {
    for (const shader of shaders)
      this.gl.attachShader(this.glProgram, shader.glShader)
  }

  link() {
    if (this._linked)
      return

    this.gl.linkProgram(this.glProgram)
    this._linked = true
  }

  use() {
    this.link()
    this.gl.useProgram(this.glProgram)
  }


  getAttribute(name) {
    if (!this._attrs[name])
      this._attrs[name] = this.gl.getAttribLocation(this.glProgram, name)

    return this._attrs[name]
  }

  getUniform(name) {
    if (!this._uniforms[name])
      this._uniforms[name] = this.gl.getUniformLocation(this.glProgram, name)

    return this._uniforms[name]
  }


  setAttributePointer(name = 'aVertexPosition', size = 3) {
    this.gl.enableVertexAttribArray(this.getAttribute(name))
    this.gl.vertexAttribPointer(
      this.getAttribute(name),
      size,
      this.gl.FLOAT,
      false,
      0,
      0
    )
  }

  setUniformMat4f(name, mat) {
    this.gl.uniformMatrix4fv(
      this.getUniform(name),
      false,
      new Float32Array(mat)
    )
  }

  setUniform2f(name, vec) {
    this.gl.uniform2fv(
      this.getUniform(name),
      new Float32Array(vec)
    )
  }

  setUniform2i(name, vec) {
    this.gl.uniform2iv(
      this.getUniform(name),
      new Int32Array(vec)
    )
  }

  setUniform1i(name, x) {
    this.gl.uniform1iv(
      this.getUniform(name),
      new Int32Array(x)
    )
  }
}

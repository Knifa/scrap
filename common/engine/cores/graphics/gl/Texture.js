export default class Texture {
  constructor(gl) {
    this.gl = gl
    this.glTexture = this.gl.createTexture()
  }

  bind() {
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.glTexture);
  }

  use(index = 0) {
    if (index < 0 || index > 30)
      throw `Out of range texture (${index})`

    this.gl.activeTexture(this.gl.TEXTURE0 + index)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.glTexture);
  }

  setDataImage(img) {
    this.bind()
    this.gl.texImage2D(
      this.gl.TEXTURE_2D,
      0,
      this.gl.RGBA,
      this.gl.RGBA,
      this.gl.UNSIGNED_BYTE,
      img
    )
  }

  setDataRaw(width, height, data) {
    this.bind()
    this.gl.texImage2D(
      this.gl.TEXTURE_2D,
      0,
      this.gl.RGBA,
      width,
      height,
      0,
      this.gl.RGBA,
      this.gl.UNSIGNED_BYTE,
      data
    )
  }

  setParameter(param, value) {
    this.bind()
    this.gl.texParameteri(this.gl.TEXTURE_2D, param, value)
  }

  generateMipmap() {
    this.bind()
    this.gl.generateMipmap(this.gl.TEXTURE_2D)
  }
}

export default class VertexBuffer {
  constructor(gl, verts) {
    this.gl = gl
    this.glBuffer = this.gl.createBuffer()

    this.bind()
    this.gl.bufferData(
      this.gl.ARRAY_BUFFER,
      new Float32Array(verts),
      this.gl.STATIC_DRAW
    )
  }

  bind() {
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.glBuffer)
  }
}

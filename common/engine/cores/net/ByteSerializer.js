const UINT8 = 0
const UINT16 = 1
const INT = 2
const FLOAT = 3

const TYPE_BYTES = {}
TYPE_BYTES[UINT8] = Uint8Array.BYTES_PER_ELEMENT
TYPE_BYTES[UINT16] = Uint16Array.BYTES_PER_ELEMENT
TYPE_BYTES[INT] = Int32Array.BYTES_PER_ELEMENT
TYPE_BYTES[FLOAT] = Float32Array.BYTES_PER_ELEMENT


export class ByteSerializerEncoder {
  constructor() {
    this._queue = []
  }

  addUInt8(v) {
    this._queue.push([UINT8, v])
  }

  addUInt16(v) {
    this._queue.push([UINT16, v])
  }

  addInt(v) {
    this._queue.push([INT, v])
  }

  addFloat(v) {
    this._queue.push([FLOAT, v])
  }

  addString(s) {
    for (const i in s) {
      this.addUInt8(s.charCodeAt(i))
    }

    this.addUInt8(0)
  }

  build() {
    const buffer = new ArrayBuffer(this._getByteLength())
    const dv = new DataView(buffer)
    let offset = 0;

    for (const item of this._queue) {
      switch (item[0]) {
        case UINT8:
          dv.setUint8(offset, item[1])
          break

        case UINT16:
          dv.setUint16(offset, item[1])
          break

        case INT:
          dv.setInt32(offset, item[1])
          break

        case FLOAT:
          dv.setFloat32(offset, item[1])
          break
      }

      offset += TYPE_BYTES[item[0]]
    }

    return buffer
  }

  _getByteLength() {
    return this._queue.reduce(
      (prev, curr) => {
        return prev + TYPE_BYTES[curr[0]]
      },
      0
    )
  }
}

export class ByteSerializerDecoder {
  constructor(buffer) {
    this._dv = new DataView(buffer)
    this._offset = 0
  }

  readUInt8() {
    const v = this._dv.getUint8(this._offset)
    this._offset += TYPE_BYTES[UINT8]
    return v
  }

  readUInt16() {
    const v = this._dv.getUint16(this._offset)
    this._offset += TYPE_BYTES[UINT16]
    return v
  }

  readInt() {
    const v = this._dv.getInt32(this._offset)
    this._offset += TYPE_BYTES[INT]
    return v
  }

  readFloat() {
    const v = this._dv.getFloat32(this._offset)
    this._offset += TYPE_BYTES[FLOAT]
    return v
  }

  readString() {
    const bytes = []

    let byte;
    while ((byte = this.readUInt8()) !== 0) {
      bytes.push(byte)
    }

    return String.fromCharCode.apply(undefined, bytes)
  }
}

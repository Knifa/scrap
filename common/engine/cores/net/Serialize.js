import Vector from 'engine/utils/Vector'
import Transform from 'engine/utils/Transform'


const _dataTypes = {}


function serialize(type, input) {
  return _dataTypes[type].serialize(input)
}

function deserialize(type, input) {
  return _dataTypes[type].deserialize(input)
}

function equal(type, inputA, inputB) {
  return _dataTypes[type].equal(inputA, inputB)
}

function encode(type, enc, input) {
  return _dataTypes[type].encode(enc, input)
}

function decode(type, dec) {
  return _dataTypes[type].decode(dec)
}

function _registerDataType(type, funcs) {
  _dataTypes[type] = funcs
}


_registerDataType(
  'number',
  {
    serialize: (input) => input,
    deserialize: (input) => input,
    equal: (inputA, inputB) => inputA == inputB,

    encode: (enc, input) => {
      enc.addFloat(input)
    },

    decode: (dec) => {
      return dec.readFloat()
    }
  }
)

_registerDataType(
  'string',
  {
    serialize: (input) => input,
    deserialize: (input) => input,
    equal: (inputA, inputB) => inputA == inputB,

    encode: (enc, input) => {
      enc.addString(input)
    },

    decode: (dec) => {
      return dec.readString()
    }
  }
)

_registerDataType(
  'vector',
  {
    serialize: (input) => [
      serialize('number', input.x),
      serialize('number', input.y)
    ],

    deserialize: (input) => new Vector({
      x: deserialize('number', input[0]),
      y: deserialize('number', input[1])
    }),

    equal: (inputA, inputB) =>
      inputA[0] == inputB[0] && inputA[1] == inputB[1],

    encode: (enc, input) => {
      enc.addFloat(input[0])
      enc.addFloat(input[1])
    },

    decode: (dec) => {
      return [dec.readFloat(), dec.readFloat()]
    }
  }
)

_registerDataType(
  'transform',
  {
    serialize: (input) => [
      serialize('vector', input.pos),
      serialize('vector', input.scale),
      serialize('number', input.rot)
    ],

    deserialize: (input) => new Transform(
      deserialize('vector', input[0]),
      deserialize('vector', input[1]),
      deserialize('number', input[2])
    ),

    equal: (inputA, inputB) =>
      equal('vector', inputA[0], inputB[0]) &&
      equal('vector', inputA[1], inputB[1]) &&
      equal('number', inputA[2], inputB[2]),

    encode: (enc, input) => {
      encode('vector', enc, input[0])
      encode('vector', enc, input[1])
      encode('number', enc, input[2])
    },

    decode: (dec) => {
      return [
        decode('vector', dec),
        decode('vector', dec),
        decode('number', dec)
      ]
    }
  }
)


export default {
  serialize,
  deserialize,
  equal,
  encode,
  decode
}

import GameCore from '../../../GameCore'
import GameEntity from '../../../GameEntity'

import Serialize from '../Serialize'
import { ByteSerializerDecoder } from '../ByteSerializer'

import WebsocketClient from './WebsocketClient'


export default class NetClientCore extends GameCore {
  constructor(game) {
    super(game)

    this.player = {
      id: null
    }

    this.game.events.on('core:start', () => this.start())
    this.game.events.on('core:tick', () => this.tick())

    this._ws = new WebsocketClient()
  }

  start() {
    this._ws.start()

    this._ws.events.on('connect', () => { console.log('Connected') })
    this._ws.events.on('snapshot', (...args) => { this.onSnapshot(...args) })
    this._ws.events.on('init', (...args) => { this.onInit(...args) })
  }

  tick() {
    this._ws.send('snapshot', {
      input: {
        mouse: this.game.cores.input.mouse,
        keys: this.game.cores.input.keys
      }
    })
  }

  onInit(dec) {
    this.player.id = dec.readUInt16()
    const entities = this._decodeSnapshot(dec)

    for (const entityData of entities) {
      this._deserializeSpawnEntity(entityData)
    }
  }

  onSnapshot(dec) {
    const spawnEntities = this._decodeSnapshot(dec)
    const deltaEntities = this._decodeSnapshot(dec)

    for (const entityData of spawnEntities) {
      this._deserializeSpawnEntity(entityData)
    }

    for (const entityData of deltaEntities) {
      this._deserializeDeltaEntity(entityData)
    }
  }

  _deserializeSpawnEntity(data) {
    const entity = new GameEntity(data.id)

    for (const [componentName, componentData] of Object.entries(data.components)) {
      entity.addComponent(
        new this.game.components[componentName](componentData)
      )
    }

    this.game.addEntity(entity)
  }

  _deserializeDeltaEntity(data) {
    const entity = this.game.entities[data.id]

    for (const [componentName, componentData] of Object.entries(data.components)) {
      for (const [propertyName, propertyValue] of Object.entries(componentData)) {
        entity.components[componentName][propertyName] = propertyValue
      }
    }
  }

  _decodeSnapshot(dec) {
    const entities = []

    const entityCount = dec.readUInt16()
    for (let i = 0; i < entityCount; i++) {
      const entity = {
        id: dec.readUInt16(),
        components: {}
      }

      const componentCount = dec.readUInt8()
      for (let j = 0; j < componentCount; j++) {
        const componentId = dec.readUInt8()
        const componentName = Object.keys(this.game.components)[componentId]

        entity.components[componentName] = {}

        const propertyCount = dec.readUInt8()
        const toSync = this.game.components[componentName].netSyncProperties

        for (let i = 0; i < propertyCount; i++) {
          const propertyIndex = dec.readUInt8()
          const propertyName = Object.keys(toSync)[propertyIndex]
          const propertyType = toSync[propertyName]

          const decoded = Serialize.decode(propertyType, dec)
          const deserialized = Serialize.deserialize(propertyType, decoded)

          entity.components[componentName][propertyName] = deserialized
        }
      }

      entities.push(entity)
    }

    return entities
  }
}


NetClientCore.friendlyName = 'net'

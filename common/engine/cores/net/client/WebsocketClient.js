import EventEmitter from 'events'
import { ByteSerializerDecoder } from '../ByteSerializer'

export default class WebsocketClient {
  constructor() {
    this._ws = null
    this.events = new EventEmitter()
  }

  start() {
    this._ws = new WebSocket('ws://me.kawaii.computer:8081', 'scrap-protocol')
    this._ws.binaryType = 'arraybuffer'

    this._ws.onopen = (...args) => { this._onOpen(...args) }
    this._ws.onmessage = (...args) => { this._onMessage(...args) }
  }

  send(type, data) {
    if (this._ws.readyState !== WebSocket.OPEN)
      return

    const message = JSON.stringify({ type, data })
    this._ws.send(message)
  }

  _onOpen() {
    this.events.emit('connect')
  }

  _onMessage(e) {
    if (e.data instanceof ArrayBuffer) {
      const dec = new ByteSerializerDecoder(e.data)
      const event = dec.readString()

      this.events.emit(event, dec)
    } else {
      const parsed = JSON.parse(e.data)
      this.events.emit(parsed.type, parsed.data)
    }
  }
}

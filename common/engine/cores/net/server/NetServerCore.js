import GameCore from '../../../GameCore'
import WebsocketServer from './WebsocketServer'

import Serialize from '../Serialize'
import { ByteSerializerEncoder } from '../ByteSerializer'


class _Player {
  constructor(id, client) {
    this.id = id
    this.client = client
    this.input = null
  }
}


export default class NetServerCore extends GameCore {
  constructor(game) {
    super(game)

    this.players = []

    this.game.events.on('core:start', () => this.start())
    this.game.events.on('core:spawnEntity', (...args) => this.spawnEntity(...args))
    this.game.events.on('core:postTick', (...args) => this.postTick(...args))

    this.server = new WebsocketServer()

    this._lastState = null
    this._queuedEntities = []
  }

  start() {
    this.server.start()

    this.server.events.on('connect', (...args) => this._onClientConnect(...args))
  }

  postTick() {
    const gameState = this._getGameState()
    let diffState

    if (this._lastState) {
      diffState = this._diffGameState(this._lastState, gameState)
    } else {
      diffState = gameState
    }

    const enc = new ByteSerializerEncoder()

    enc.addUInt16(this._queuedEntities.length)
    for (const e of this._queuedEntities)
      this._encodeEntity(e, enc)

    this._encodeGameState(enc, diffState)

    const buf = enc.build()
    console.log(buf.byteLength)
    for (const client of this.server.clients) {
      client.sendBinary('snapshot', buf)
    }

    this._queuedEntities = []
    this._lastState = gameState
  }

  spawnEntity(entity) {
    this._queuedEntities.push(entity)
  }

  _onClientConnect(client) {
    const player = new _Player(this.players.length, client)
    this.players.push(player)

    const enc = new ByteSerializerEncoder()
    enc.addUInt16(player.id)
    enc.addUInt16(this.game.entities.length)
    for (const e of this.game.entities)
      this._encodeEntity(e, enc)
    client.sendBinary('init', enc.build())

    client.events.on(
      'snapshot',
      (data) => { this._onClientSnapshot(player, data) }
    )

    this.game.events.emit('net:playerConnect', player)
  }

  _onClientSnapshot(player, data) {
    player.input = data.input
  }

  _encodeEntity(entity, enc) {
    enc.addUInt16(entity.id)
    enc.addUInt8(Object.keys(entity.components).length)

    for (const [componentType, component] of Object.entries(entity.components)) {
      const typeIndex = Object.keys(this.game.components).findIndex(
        (k) => k === componentType
      )

      enc.addUInt8(typeIndex)

      const toSync = this.game.components[componentType].netSyncProperties
      if (!toSync) {
        enc.addUInt8(0)
        continue
      }

      enc.addUInt8(Object.keys(toSync).length)

      for (const [property, propertyType] of Object.entries(toSync)) {
        const propertyIndex = Object.keys(toSync).findIndex(
          (k) => k === property
        )
        enc.addUInt8(propertyIndex)

        Serialize.encode(
          propertyType,
          enc,
          Serialize.serialize(
            propertyType,
            component[property]
          )
        )
      }
    }
  }

  _getGameState() {
    const entities = {}

    for (const entity of this.game.entities) {
      const entityData = {}

      for (const [componentType, component] of Object.entries(entity.components)) {
        entityData[componentType] = {}

        const toSync = this.game.components[componentType].netSyncProperties
        if (!toSync) {
          continue
        }

        for (const [property, propertyType] of Object.entries(toSync)) {
          entityData[componentType][property] = Serialize.serialize(
              propertyType,
              component[property]
            )
        }
      }

      entities[entity.id] = entityData
    }

    return {
      entities
    }
  }

  _diffGameState(oldState, newState) {
    const diffState = {
      entities: {}
    }

    for (const entityId in newState.entities) {
      if (!(entityId in oldState.entities)) {
        diffState.entities[entityId] = newState.entities[entityId]
        continue
      }

      const oldComponents = oldState.entities[entityId]
      const newComponents = newState.entities[entityId]

      for (const [componentType, component] of Object.entries(newComponents)) {
        if (Object.keys(component).length === 0)
          continue

        const toSync = this.game.components[componentType].netSyncProperties

        for (const [property, value] of Object.entries(component)) {
          if (!Serialize.equal(toSync[property], value, oldComponents[componentType][property])) {
            if (!(entityId in diffState.entities))
              diffState.entities[entityId] = {}

            if (!(componentType in diffState.entities[entityId]))
              diffState.entities[entityId][componentType] = {}

            diffState.entities[entityId][componentType][property] = value
          }
        }
      }
    }

    return diffState
  }

  _encodeGameState(enc, state) {
    enc.addUInt16(Object.keys(state.entities).length)

    for (const [entityId, components] of Object.entries(state.entities)) {
      enc.addUInt16(entityId)
      enc.addUInt8(Object.keys(components).length)

      for (const [componentType, component] of Object.entries(components)) {
        const typeIndex = Object.keys(this.game.components).findIndex(
          (k) => k === componentType
        )
        enc.addUInt8(typeIndex)

        enc.addUInt8(Object.keys(component).length)
        if (Object.keys(component).length === 0)
          continue

        const toSync = this.game.components[componentType].netSyncProperties
        for (const [property, value] of Object.entries(component)) {
          const propertyIndex = Object.keys(toSync).findIndex(
            (k) => k === property
          )
          enc.addUInt8(propertyIndex)

          Serialize.encode(toSync[property], enc, value)
        }
      }
    }
  }
}


NetServerCore.friendlyName = 'net'

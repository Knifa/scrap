import ws from 'ws'
import EventEmitter from 'events'


class _WebsocketClient {
  constructor(socket) {
    this._ws = socket
    this.events = new EventEmitter()

    this._ws.on('message', (...args) => { this._onMessage(...args) })
  }

  send(type, data) {
    if (this._ws.readyState !== ws.OPEN)
      return

    const message = JSON.stringify({ type, data })
    this._ws.send(message)
  }

  sendBinary(type, buffer) {
    if (this._ws.readyState !== ws.OPEN)
      return

    const string = new ArrayBuffer(type.length + 1)
    const msg = new ArrayBuffer(string.byteLength + buffer.byteLength)

    const stringView = new DataView(string)
    const msgView = new DataView(msg)
    const dataView = new DataView(buffer)

    for (const i in type) {
      stringView.setUint8(i, type.charCodeAt(i))
    }

    for (let i = 0; i < stringView.byteLength; i++) {
      msgView.setUint8(i, stringView.getUint8(i))
    }

    for (let i = 0; i < dataView.byteLength; i++)
      msgView.setUint8(i + string.byteLength, dataView.getUint8(i))

    this._ws.send(msg, { binary: true, mask: false })
  }

  _onMessage(message) {
    const parsed = JSON.parse(message)
    this.events.emit(parsed.type, parsed.data)
  }
}


export default class WebsocketServer {
  constructor() {
    this._ws = null
    this.clients = []

    this.events = new EventEmitter()
  }

  start() {
    this._ws = new ws.Server({
      host: '0.0.0.0',
      port: 8081
    })

    this._ws.on('connection', (...args) => { this._onConnection(...args) })
  }

  _onConnection(socket) {
    const client = new _WebsocketClient(socket)
    this.clients.push(client)
    this.events.emit('connect', client)
  }
}

import GameSystem from 'engine/GameSystem'

import AttachComponent from 'engine/components/AttachComponent'
import TransformComponent from 'engine/components/TransformComponent'


export default class AttachSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:postTick', (...args) => this.postTick(...args))
  }

  postTick() {
    const entities = this.game.getEntitiesWithComponents([
      AttachComponent,
      TransformComponent
    ])

    for (const e of entities) {
      if (e.attach.parentId) {
        const parent = this.game.entities[e.attach.parentId]

        e.transform.world.pos =
          parent.transform.world.pos
            .add(e.transform.local.pos
              .rotate(parent.transform.world.rot)
            )
      }
    }
  }

  /*static attachChild(childId, parentId) {
    AttachSystem._ensureHasAttachComponent(child)
    AttachSystem._ensureHasAttachComponent(parent)

    child.attach.parentId = parentId
    parent.attach.children.push(childId)
  }

  static _ensureHasAttachComponent(entity) {
    if (!entity.hasComponent(AttachComponent))
      entity.addComponent(new AttachComponent())
  }*/
}

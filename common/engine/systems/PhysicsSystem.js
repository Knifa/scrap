import GameSystem from '../GameSystem'

import PhysicsComponent from '../components/PhysicsComponent'
import TransformComponent from '../components/TransformComponent'

import Vector from '../utils/Vector'


export default class PhysicsSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', (...args) => this.tick(...args))
  }

  tick(time) {
    const entities = this.game.getEntitiesWithComponents([
      PhysicsComponent,
      TransformComponent
    ])

    for (const e of entities) {
      let acceleration = new Vector()
      for (const f of e.physics.forces) {
        acceleration = acceleration
          .add(f.mul(e.physics.mass))
      }

      e.physics.velocity = e.physics.velocity
        .add(acceleration.mul(time.dt))

      e.transform.world.pos = e.transform.world.pos
        .add(e.physics.velocity.mul(time.dt))

      e.physics.forces = []
    }
  }
}

import GameSystem from '../GameSystem'
import RunMode from '../RunMode'
import BlendFunc from '../cores/graphics/gl/BlendFunc'

import SpriteComponent from '../components/SpriteComponent'
import TransformComponent from '../components/TransformComponent'

import Transform from '../utils/Transform'


export default class SpriteRenderingSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:render', () => this.render())
  }

  render() {
    const entities = this.game.getEntitiesWithComponents([
      SpriteComponent,
      TransformComponent
    ])

    for (const entity of entities) {
      this._render(entity)
    }
  }

  _render(entity) {
    const sprite = entity.sprite
    const transform = entity.transform

    this.game.cores.gfx.glResources.getVertexBuffer('sprite').bind()
    this.game.cores.gfx.glResources.getTexture(sprite.texturePath).use()

    const shaderProg = this.game.cores.gfx.glResources.getShaderProgram(
      sprite.shaderPath)

    shaderProg.use()
    shaderProg.setAttributePointer()

    shaderProg.setUniform1i('uTexture', [0])
    shaderProg.setUniform2f('uScale', sprite.textureScale.toArray())
    shaderProg.setUniform2f('uOffset', sprite.textureOffset.toArray())

    shaderProg.setUniformMat4f('uPMatrix',
      this.game.cores.gfx.camera.getProjectionMatrix())
    shaderProg.setUniformMat4f('uVMatrix',
      this.game.cores.gfx.camera.getViewMatrix())
    shaderProg.setUniformMat4f('uMVMatrix',
      this._getModelViewMatrix(transform.world, sprite.size))

    shaderProg.setUniform2i('uResolution',
      [this.game.cores.gfx.canvas.width, this.game.cores.gfx.canvas.height])

    BlendFunc.apply(this.game.cores.gfx.gl, sprite.blendFunc)
    this.game.cores.gfx.gl.drawArrays(
      this.game.cores.gfx.gl.TRIANGLE_STRIP, 0, 4)
  }

  _getModelViewMatrix(transform, size) {
    let actualSize = size
    if (size.x === -1 && size.y === -1)
      actualSize = this.game.cores.gfx.canvasSize.clone()

    return new Transform(
      transform.pos,
      transform.scale.mulComponent(actualSize),
      transform.rot
    ).toMatrix()
  }
}

SpriteRenderingSystem.runsOn = RunMode.CLIENT

import GameSystem from 'engine/GameSystem'
import BlendFunc from 'engine/cores/graphics/gl/BlendFunc'

import TrailComponent from 'engine/components/TrailComponent'
import TransformComponent from 'engine/components/TransformComponent'

import { mat4 } from 'gl-matrix'


export default class TrailSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', (...args) => this.tick(...args))
    this.game.events.on('system:render', (...args) => this.render(...args))

    this._lastTime = 0
  }

  tick(time) {
    let shouldAdd = false
    if (time.t >= this._lastTime + 0.1) {
      this._lastTime = time.t
      shouldAdd = true
    }

    const entities = this.game.getEntitiesWithComponents([
      TrailComponent,
      TransformComponent
    ])

    for (const e of entities) {
      if (e.trail._lastPos && shouldAdd) {
        const dirV = e.transform.world.pos.sub(e.trail._lastPos).norm().rotate(Math.PI / 2)

        if (e.trail.active) {
          if (!e.trail._lastActive && e.trail._verts.length > 0) {
            e.trail._verts.push(e.trail._verts[e.trail._verts.length - 1])
            e.trail._uv.push([0, 0.5])
            e.trail._cols.push(Array.from(e.trail.color))

            e.trail._verts.push(e.transform.world.pos.toArray())
            e.trail._uv.push([0, 0.5])
            e.trail._cols.push(Array.from(e.trail.color))

            e.trail._verts.push(e.transform.world.pos.toArray())
            e.trail._uv.push([0, 0.5])
            e.trail._cols.push(Array.from(e.trail.color))
          }

          e.trail._verts.push(
            e.transform.world.pos
              .add(dirV.mul(8))
              .toArray()
          )

          e.trail._uv.push([0, 0.5])
          e.trail._cols.push(Array.from(e.trail.color))

          e.trail._verts.push(
            e.transform.world.pos
              .add(dirV.mul(-8))
              .toArray()
          )

          e.trail._uv.push([1, 0.5])
          e.trail._cols.push(Array.from(e.trail.color))
        }
      }

      const prunedVerts = []
      const prunedUvs = []
      const prunedCols = []

      for (const [index, col] of Object.entries(e.trail._cols)) {
        col[3] -= time.dt

        if (col[3] > 0) {
          prunedVerts.push(e.trail._verts[index])
          prunedUvs.push(e.trail._uv[index])
          prunedCols.push(e.trail._cols[index])
        }
      }

      e.trail._verts = prunedVerts
      e.trail._uv = prunedUvs
      e.trail._cols = prunedCols

      e.trail._lastPos = e.transform.world.pos.clone()
      e.trail._lastActive = e.trail.active
    }
  }

  render() {
    const gfx = this.game.cores.gfx
    const gl = gfx.gl

    const entities = this.game.getEntitiesWithComponents([
      TrailComponent,
      TransformComponent
    ])

    for (const e of entities) {
      if (!e.trail._vertBuffer && !e.trail._uvBuffer && !e.trail._colBuffer) {
        e.trail._vertBuffer = gl.createBuffer()
        e.trail._uvBuffer = gl.createBuffer()
        e.trail._colBuffer = gl.createBuffer()
      }

      if (e.trail._verts.length >= 4) {
        const glVerts = []
        for (const v of e.trail._verts) {
          glVerts.push(...v, 0)
        }

        const glUv = []
        for (const uv of e.trail._uv) {
          glUv.push(...uv)
        }

        const glCol = []
        for (const col of e.trail._cols) {
          glCol.push(...col)
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._vertBuffer)
        gl.bufferData(
          gl.ARRAY_BUFFER,
          new Float32Array(glVerts),
          gl.DYNAMIC_DRAW
        )

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._uvBuffer)
        gl.bufferData(
          gl.ARRAY_BUFFER,
          new Float32Array(glUv),
          gl.DYNAMIC_DRAW
        )

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._colBuffer)
        gl.bufferData(
          gl.ARRAY_BUFFER,
          new Float32Array(glCol),
          gl.DYNAMIC_DRAW
        )

        gfx.glResources.getTexture('trail.jpg').use()
        const shaderProg = gfx.glResources.getShaderProgram('trail')
        shaderProg.use()

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._vertBuffer)
        shaderProg.setAttributePointer()

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._uvBuffer)
        shaderProg.setAttributePointer('aVertexUv', 2)

        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._colBuffer)
        shaderProg.setAttributePointer('aVertexColor', 4)

        shaderProg.setUniform1i('uTexture', [0])
        shaderProg.setUniformMat4f('uPMatrix', gfx.camera.getProjectionMatrix())
        shaderProg.setUniformMat4f('uVMatrix', gfx.camera.getViewMatrix())
        shaderProg.setUniformMat4f('uMVMatrix', mat4.create())

        BlendFunc.apply(gl, BlendFunc.ADD)
        gl.bindBuffer(gl.ARRAY_BUFFER, e.trail._vertBuffer)
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, glVerts.length / 3)
      }
    }
  }
}

import Vector from './Vector'
import { mat4 } from 'gl-matrix'

export default class Transform {
  constructor(pos, scale, rot) {
    this.pos = pos ? pos.clone() : new Vector()
    this.scale = scale ? scale.clone() : new Vector({ x: 1, y: 1 })
    this.rot = rot ? rot : 0
  }

  toMatrix() {
    const m = mat4.create()
    mat4.translate(m, m, [this.pos.x, this.pos.y, 0])
    mat4.scale(m, m, [this.scale.x, this.scale.y, 1])
    mat4.rotateZ(m, m, this.rot)

    return m
  }

  relativeTo(t) {
    return new Transform(
      t.pos.sub(this.pos.rotate(t.rot)),
      this.scale.mulComponent(t.scale),
      this.rot + t.rot
    )
  }

  clone() {
    return new Transform(this.pos, this.scale, this.rot)
  }
}

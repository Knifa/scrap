export default class Vector {
  constructor({ x, y } = { x: 0, y: 0 }) {
    this.x = x
    this.y = y

    this._lazyMag = undefined;
    this._lazyAngle = undefined;
  }

  clone() {
    return new Vector(this)
  }


  add(v) {
    return new Vector({
      x: this.x + v.x,
      y: this.y + v.y
    })
  }

  sub(v) {
    return new Vector({
      x: this.x - v.x,
      y: this.y - v.y
    })
  }

  mul(s) {
    return new Vector({
      x: this.x * s,
      y: this.y * s
    })
  }

  mulComponent(v) {
    return new Vector({
      x: this.x * v.x,
      y: this.y * v.y
    })
  }

  neg() {
    return new Vector({
      x: -this.x,
      y: -this.y
    })
  }

  norm() {
    const mag = this.mag()
    if (mag == 0) {
      return new Vector()
    }

    return new Vector({
      x: this.x / mag,
      y: this.y / mag
    })
  }


  mag() {
    if (!this._lazyMag)
      this._lazyMag = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))

    return this._lazyMag
  }

  dot(v) {
    return this.x * v.x + this.y * v.y
  }

  cross(v) {
    return this.x * v.y - this.y * v.x
  }


  angle() {
    if (!this._lazyAngle)
      this._lazyAngle = Math.atan2(this.y, this.x)

    return this._lazyAngle
  }

  angleBetween(v) {
    if (this.mag() == 0 || v.mag() == 0)
      return 0

    return Math.acos(this.dot(v) / (this.mag() * v.mag()))
  }

  rotate(t) {
    return new Vector({
      x: Math.cos(t) * this.x - Math.sin(t) * this.y,
      y: Math.sin(t) * this.x + Math.cos(t) * this.y
    })
  }

  dir(v) {
    return Math.sign(this.cross(v))
  }

  toArray() {
    return [this.x, this.y]
  }

  static fromAngle(t) {
    return new Vector({
      x: Math.cos(t),
      y: Math.sin(t)
    })
  }
}

import GameComponent from 'engine/GameComponent'

export default class BackgroundScrollComponent extends GameComponent {
  constructor(options = { multiplier: 1 }) {
    super()

    this.multiplier = options.multiplier
  }
}

BackgroundScrollComponent.netSyncProperties = {
  multiplier: 'number'
}

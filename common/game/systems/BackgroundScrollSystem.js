import GameSystem from 'engine/GameSystem'
import RunMode from 'engine/RunMode'

import BackgroundScrollComponent from 'game/components/BackgroundScrollComponent'
import SpriteComponent from 'engine/components/SpriteComponent'


export default class BackgroundScrollSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', (...args) => this.tick(...args))
  }

  tick() {
    const entities = this.game.getEntitiesWithComponents([
      BackgroundScrollComponent,
      SpriteComponent
    ])

    for (const e of entities) {
      e.sprite.textureOffset = this.game.cores.gfx.camera.transform.pos
        .neg()
        .mul(e.backgroundScroll.multiplier)
    }
  }
}

BackgroundScrollSystem.needs = [
  BackgroundScrollComponent,
  SpriteComponent
]

BackgroundScrollSystem.runsOn = RunMode.CLIENT

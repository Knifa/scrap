import GameSystem from 'engine/GameSystem'
import RunMode from 'engine/RunMode'

import TransformComponent from 'engine/components/TransformComponent'
import OwnedByPlayerComponent from 'engine/components/OwnedByPlayerComponent'
import CameraFollowerComponent from 'game/components/CameraFollowerComponent'


export default class CameraFollowerSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', (...args) => this.tick(...args))
  }

  tick() {
    const entities = this.game.getEntitiesWithComponents([
      TransformComponent,
      CameraFollowerComponent
    ])

    for (const e of entities) {
      if (e.ownedByPlayer.playerId === this.game.cores.net.player.id)
        this.game.cores.gfx.camera.lookAt(e.transform.world.pos)
    }
  }
}

CameraFollowerSystem.needs = [
  TransformComponent,
  CameraFollowerComponent,
  OwnedByPlayerComponent
]

CameraFollowerSystem.runsOn = RunMode.CLIENT

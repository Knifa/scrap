import GameSystem from 'engine/GameSystem'
import RunMode from 'engine/RunMode'

import TransformComponent from 'engine/components/TransformComponent'
import OrbitComponent from 'game/components/OrbitComponent'
import OwnedByPlayerComponent from 'engine/components/OwnedByPlayerComponent'


export default class OrbitSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', () => this.tick())
  }

  tick() {
    const entities = this.game.getEntitiesWithComponents([
      TransformComponent,
      OrbitComponent,
      OwnedByPlayerComponent
    ])

    for (const entity of entities) {
      const player = this.game.cores.net.players[entity.ownedByPlayer.playerId]
      if (!player.input)
        continue

      entity.orbit._t += 0.01
      entity.transform.world.pos.x = -player.input.mouse.x
      entity.transform.world.pos.y = -player.input.mouse.y
      entity.transform.world.rot = entity.orbit._t
    }
  }
}

OrbitSystem.runsOn = RunMode.SERVER

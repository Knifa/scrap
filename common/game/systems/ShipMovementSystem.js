import GameSystem from 'engine/GameSystem'
import RunMode from 'engine/RunMode'

import PhysicsComponent from 'engine/components/PhysicsComponent'
import TransformComponent from 'engine/components/TransformComponent'
import TrailComponent from 'engine/components/TrailComponent'
import OwnedByPlayerComponent from 'engine/components/OwnedByPlayerComponent'
import ShipMovementComponent from 'game/components/ShipMovementComponent'

import Vector from 'engine/utils/Vector'


export default class ShipMovementSystem extends GameSystem {
  constructor(game) {
    super(game)

    this.game.events.on('system:tick', (...args) => this.tick(...args))

    this._lastFireTime = 0
  }

  tick(time) {
    const entities = this.game.getEntitiesWithComponents([
      PhysicsComponent,
      TransformComponent,
      ShipMovementComponent,
      OwnedByPlayerComponent
    ])

    for (const entity of entities) {
      const player = this.game.cores.net.players[entity.ownedByPlayer.playerId]
      if (!player.input)
        continue

      entity.transform.world.rot = Math.atan2(
        player.input.mouse.y,
        player.input.mouse.x
      ) - Math.PI / 2

      if (player.input.mouse.buttonLeft) {
        entity.physics.forces.push(new Vector({
          x: Math.cos(entity.transform.world.rot - Math.PI / 2),
          y: Math.sin(entity.transform.world.rot - Math.PI / 2)
        }).mul(500))
      } else {
        if (entity.physics.velocity.mag() > 1) {
          entity.physics.forces.push(
            entity.physics.velocity.neg().norm().mul(250))
        }
      }

      if (player.input.keys[32] && time.t >= this._lastFireTime + 0.01) {
        const bulletId = this.game.spawnEntity([
          new TransformComponent(),
          new PhysicsComponent(),
          new TrailComponent()
        ])

        const bullet = this.game.entities[bulletId]
        bullet.transform.world.pos = entity.transform.world.pos.clone()
        bullet.physics.velocity = new Vector({
          x: Math.cos(entity.transform.world.rot - Math.PI / 2),
          y: Math.sin(entity.transform.world.rot - Math.PI / 2)
        }).mul(1000)

        this._lastFireTime = time.t
      }
    }
  }
}

ShipMovementSystem.runsOn = RunMode.SERVER

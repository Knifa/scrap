import Game from 'engine/Game'
import RunMode from 'engine/RunMode'

import NetServerCore from 'engine/cores/net/server/NetServerCore'

import TransformComponent from 'engine/components/TransformComponent'
import SpriteComponent from 'engine/components/SpriteComponent'
import OwnedByPlayerComponent from 'engine/components/OwnedByPlayerComponent'
import PhysicsComponent from 'engine/components/PhysicsComponent'
import AttachComponent from 'engine/components/AttachComponent'
import TrailComponent from 'engine/components/TrailComponent'

import PhysicsSystem from 'engine/systems/PhysicsSystem'
import AttachSystem from 'engine/systems/AttachSystem'

import CameraFollowerComponent from 'game/components/CameraFollowerComponent'
import ShipMovementComponent from 'game/components/ShipMovementComponent'
import BackgroundScrollComponent from 'game/components/BackgroundScrollComponent'

import ShipMovementSystem from 'game/systems/ShipMovementSystem'

import Vector from 'engine/utils/Vector'


const game = new Game({
  runMode: RunMode.SERVER,

  cores: [
    NetServerCore
  ],

  systems: [
    PhysicsSystem,
    ShipMovementSystem,
    AttachSystem
  ],

  components: [
    TransformComponent,
    SpriteComponent,
    OwnedByPlayerComponent,
    PhysicsComponent,
    ShipMovementComponent,
    CameraFollowerComponent,
    BackgroundScrollComponent,
    AttachComponent,
    TrailComponent
  ]
})

game.start()

game.spawnEntity([
  new TransformComponent(),
  new BackgroundScrollComponent({ multiplier: 0.05 }),
  new SpriteComponent({
    texturePath: 'nebula2.jpg',
    shaderPath: 'sprite_screenspace',
    blendFunc: 'add',
    size: SpriteComponent.FULLSCREEN,
    textureScale: new Vector({
      x: 0.5,
      y: 0.5
    })
  })
])

game.spawnEntity([
  new TransformComponent(),
  new BackgroundScrollComponent({ multiplier: 0.1 }),
  new SpriteComponent({
    texturePath: 'nebula1.jpg',
    shaderPath: 'sprite_screenspace',
    blendFunc: 'add',
    size: SpriteComponent.FULLSCREEN,
    textureScale: new Vector({
      x: 0.5,
      y: 0.5
    })
  })
])

game.spawnEntity([
  new TransformComponent(),
  new BackgroundScrollComponent({ multiplier: 0.15 }),
  new SpriteComponent({
    texturePath: 'stars.jpg',
    shaderPath: 'sprite_screenspace',
    blendFunc: 'add',
    size: SpriteComponent.FULLSCREEN,
    textureScale: new Vector({
      x: 0.5,
      y: 0.5
    })
  })
])

game.spawnEntity([
  new TransformComponent(),
  new SpriteComponent({
    texturePath: 'grid.png',
    shaderPath: 'sprite',
    blendFunc: 'add',
    size: new Vector({ x: 8192, y: 8192 }),
    textureScale: new Vector({ x: 1 / 8192 * 512, y: 1 / 8192 * 512 })
  })
])

game.events.on('net:playerConnect', (player) => {
  const ship = game.spawnEntity([
    new TransformComponent(),
    new PhysicsComponent(),
    new OwnedByPlayerComponent({ playerId: player.id }),
    new ShipMovementComponent(),
    new CameraFollowerComponent(),
    new SpriteComponent({
      texturePath: 'ship.png',
      shaderPath: 'sprite',
      size: new Vector({ x: 128, y: 128 })
    })
  ])

  const trailLeft = game.spawnEntity([
    new TransformComponent(),
    new AttachComponent({ parentId: ship }),
    new TrailComponent()
  ])

  game.entities[trailLeft].transform.local.pos.x -= 24
  game.entities[trailLeft].transform.local.pos.y += 24

  const trailRight = game.spawnEntity([
    new TransformComponent(),
    new AttachComponent({ parentId: ship }),
    new TrailComponent()
  ])

  game.entities[trailRight].transform.local.pos.x += 24
  game.entities[trailRight].transform.local.pos.y = 24
})

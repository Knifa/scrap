const webpack = require('webpack')
const path = require('path')

module.exports = {
  context: path.resolve(__dirname, './src/js'),
  entry: ['babel-polyfill', path.resolve(__dirname, './src/index.js')],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './bin')
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
        query: {
          presets: require.resolve('babel-preset-es2015')
        }
      }
    ]
  },

  externals: [require('webpack-node-externals')()],

  resolve: {
    root: [
      path.resolve(__dirname, './src'),
      path.resolve(__dirname, '../common'),
      path.resolve(__dirname, './node_modules'),
    ]
  },

  plugins: [
    new webpack.IgnorePlugin(/^(texture|shader|mesh)/)
  ],

  devtool: 'source-map',
  target: 'node'
}
